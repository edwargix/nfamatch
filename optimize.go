package main

func (dfa *DFA) MergeStates() bool {
	M := make([]Set, 0)
	L := NewStack()

	L.Push([2]Set{dfa.AcceptingStates(), dfa.sigma.Copy()})
	L.Push([2]Set{dfa.NonAcceptingStates(), dfa.sigma.Copy()})

	for L.Size() > 0 {
		l, _ := L.Pop()
		S := l.([2]Set)[0]
		C := l.([2]Set)[1]

		c := C.Pop().(byte)

		X := make(map[StateID]Set)

		for s := range S {
			k := dfa.states[s.(StateID)].trans[c]
			if X[k] == nil {
				X[k] = make(Set)
			}
			X[k].Add(s)
		}

		for _, x := range X {
			if x.Size() > 1 {
				if C.Size() == 0 {
					contains := false
					for _, m := range M {
						if m.Equals(S) {
							contains = true
						}
					}
					if !contains {
						M = append(M, S)
					}
				} else {
					L.Push([2]Set{x, C.Copy()})
				}
			}
		}
	}

	changed := false

	for _, S := range M {
		if S.Size() > 1 {
			changed = true
		}
		canonicalID := S.Pop().(StateID)
		starting := dfa.states[canonicalID].starting
		for t := range S {
			if dfa.states[t.(StateID)].starting {
				starting = true
			}
			delete(dfa.states, t.(StateID))
			for _, meta := range dfa.states {
				for c, q := range meta.trans {
					if q == t {
						meta.trans[c] = canonicalID
					}
				}
			}
		}
		dfa.states[canonicalID].starting = starting
		if starting {
			dfa.RenameState(canonicalID, 0)
		}
	}

	return changed
}

func (dfa *DFA) RenameState(oldID StateID, newID StateID) {
	if oldID == newID {
		return
	}
	for _, stateMeta := range dfa.states {
		for c, transID := range stateMeta.trans {
			if transID == oldID {
				stateMeta.trans[c] = newID
			}
		}
	}
	dfa.states[newID] = dfa.states[oldID]
	delete(dfa.states, oldID)
}

func (dfa *DFA) DeleteState(stateID StateID) {
	delete(dfa.states, stateID)
	for _, otherMeta := range dfa.states {
		for c, transID := range otherMeta.trans {
			if transID == stateID {
				delete(otherMeta.trans, c)
			}
		}
	}
}

func (dfa *DFA) DeleteUnreachables() {
	found := make(Set)
	next := NewStack()

	next.Push(StateID(0))
	found.Add(StateID(0))

	for next.Size() > 0 {
		s, _ := next.Pop()
		for _, otherID := range dfa.states[s.(StateID)].trans {
			if !found.Contains(otherID) {
				found.Add(otherID)
				next.Push(otherID)
			}
		}
	}

	for stateID := range dfa.states {
		if !found.Contains(stateID) {
			dfa.DeleteState(stateID)
		}
	}
}

func (dfa *DFA) StateCanReachAccepting(stateID StateID) bool {
	if dfa.states[stateID].accepting {
		return true
	}

	searched := make(Set)
	next := NewStack()

	searched.Add(stateID)
	next.Push(stateID)

	for next.Size() > 0 {
		s, _ := next.Pop()
		for _, otherID := range dfa.states[s.(StateID)].trans {
			if dfa.states[otherID].accepting {
				return true
			}
			if !searched.Contains(otherID) {
				searched.Add(otherID)
				next.Push(otherID)
			}
		}
	}

	return false
}

func (dfa *DFA) DeleteUseless() {
	useless := make(Set)

	for stateID, _ := range dfa.states {
		if !dfa.StateCanReachAccepting(stateID) {
			useless.Add(stateID)
		}
	}

	for stateID := range useless {
		dfa.DeleteState(stateID.(StateID))
	}
}

func (dfa *DFA) Optimize() {
	// merge states until no further changes occur
	for dfa.MergeStates() {}

	dfa.DeleteUnreachables()

	dfa.DeleteUseless()
}
