package main

import (
	"flag"
	"fmt"
	"os"
	"path/filepath"
	"strings"
)

func graphvizFilePrefix(dfaFile string) string {
	baseName := filepath.Base(dfaFile)
	return strings.TrimSuffix(baseName, filepath.Ext(baseName))
}

func main() {
	makeGraphviz := flag.Bool("g", false, "whether graphviz files should be created for the NFA, DFA, and optimized DFA")
	flag.Parse()

	args := flag.Args()

	if len(args) < 2 {
		fmt.Fprintf(os.Stderr, "%s: too few arguments\n", os.Args[0])
		os.Exit(-1)
	}

	nfa := readNFA(args[0])
	dfa := nfa2dfa(nfa)

	if *makeGraphviz {
		WriteNFA(nfa, graphvizFilePrefix(args[0]) + "_nfa.gv")
		WriteDFA(dfa, graphvizFilePrefix(args[0]) + "_dfa.gv")
	}

	dfa.Optimize();

	if *makeGraphviz {
		WriteDFA(dfa, graphvizFilePrefix(args[0]) + "_optimized_dfa.gv")
	}

	dfaTable(dfa, args[1])

	for i := 2; i < len(args); i++ {
		printMatch(dfa, args[i])
	}
}
