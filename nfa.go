package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type (
	NFA struct {
		lambda	byte
		sigma	Set
		states	map[StateID]*NFAStateMeta
	}

	StateID int

	NFAStateMeta struct {
		trans		map[byte][]StateID
		accepting	bool
		starting	bool
	}
)

func readNFA(fname string) NFA {
	file, err := os.Open(fname);
	if err != nil {
		fmt.Fprintln(os.Stderr, err);
		os.Exit(-1)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	var fields []string

	// need at least one line
	if scanner.Scan() {
		fields = strings.Fields(scanner.Text())
	} else {
		fmt.Fprintf(os.Stderr,
			"%s: NFA defn file needs at least one line\n",
			os.Args[0])
		os.Exit(-1)
	}

	// number of states
	numStates, err := strconv.Atoi(fields[0])
	if err != nil {
		fmt.Fprintln(os.Stderr, err);
		os.Exit(-1)
	}

	// lambda; we need not use runes since every NFA file will be ASCII only
	lambda := fields[1][0]

	// alphabet
	sigma := make(Set)

	states := make(map[StateID]*NFAStateMeta, numStates)

	for i := 2; i < len(fields); i++ {
		sigma.Add(fields[i][0])
	}

	// populate states
	for scanner.Scan() {
		fields = strings.Fields(scanner.Text())
		if len(fields) == 0 {
			continue
		}
		from, err := strconv.Atoi(fields[1])
		if err != nil {
			fmt.Fprintln(os.Stderr, err)
		}
		if states[StateID(from)] == nil {
			states[StateID(from)] = &NFAStateMeta{
				trans: make(map[byte][]StateID),
			}
		}
		if from == 0 {
			states[StateID(from)].starting = true
		} else {
			states[StateID(from)].starting = false
		}
		if fields[0] == "+" {
			states[StateID(from)].accepting = true
		} else {
			states[StateID(from)].accepting = false
		}
		to, err := strconv.Atoi(fields[2])
		if err != nil {
			fmt.Fprintln(os.Stderr, err)
		}
		if states[StateID(to)] == nil {
			states[StateID(to)] = &NFAStateMeta{
				trans: make(map[byte][]StateID),
			}
		}
		trans := states[StateID(from)].trans
		for i := 3; i < len(fields); i++ {
			char := fields[i][0]
			sl := trans[char]
			if sl == nil {
				sl = make([]StateID, 0, 1)
			}
			trans[char] = append(sl, StateID(to))
		}
	}

	nfa := NFA {lambda: lambda, sigma: sigma, states: states}

	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(-1)
	}

	return nfa
}
