package main

import "fmt"

func printMatch(dfa DFA, str string) {
	i := 0

	s := dfa.states[0]

	for ; i < len(str); i++ {
		if otherID, ok := s.trans[str[i]]; ok {
			s = dfa.states[otherID]
		} else {
			fmt.Printf("OUTPUT %d\n", i + 1)
			return;
		}
	}

	if s.accepting {
		fmt.Println("OUTPUT :M:")
	} else {
		if i == 0 {
			fmt.Printf("OUTPUT %d\n", i)
		} else {
			fmt.Printf("OUTPUT %d\n", i + 1)
		}
	}
}
