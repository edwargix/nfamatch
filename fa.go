package main

func (nfa NFA) AcceptingStates() Set {
	A := make(Set)
	for id, stateMeta := range nfa.states {
		if stateMeta.accepting {
			A[StateID(id)] = struct{}{}
		}
	}
	return A
}

func (dfa DFA) AcceptingStates() Set {
	A := make(Set)
	for id, stateMeta := range dfa.states {
		if stateMeta.accepting {
			A[StateID(id)] = struct{}{}
		}
	}
	return A
}

func (nfa NFA) NonAcceptingStates() Set {
	A := make(Set)
	for id, stateMeta := range nfa.states {
		if !stateMeta.accepting {
			A[StateID(id)] = struct{}{}
		}
	}
	return A
}

func (dfa DFA) NonAcceptingStates() Set {
	A := make(Set)
	for id, stateMeta := range dfa.states {
		if !stateMeta.accepting {
			A[StateID(id)] = struct{}{}
		}
	}
	return A
}
