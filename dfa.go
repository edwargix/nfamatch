package main

import (
	"bufio"
	"fmt"
	"os"
	"sort"
)

type (
	DFA struct {
		sigma	Set
		states	map[StateID]*DFAStateMeta
	}

	DFAStateMeta struct {
		trans		map[byte]StateID
		accepting	bool
		starting	bool
	}
)

func (nfa NFA) FollowLambda(S Set) Set {
	L := NewStack()

	for t := range S {
		L.Push(t)
	}

	for L.Size() > 0 {
		t, _ := L.Pop()  // err will never be thrown
		for _, q := range nfa.states[t.(StateID)].trans[nfa.lambda] {
			if !S.Contains(q) {
				S.Add(q)
				L.Push(q)
			}
		}
	}

	return S
}

func (nfa NFA) FollowChar(S Set, c byte) Set {
	F := make(Set)

	for t := range S {
		for _, q := range nfa.states[t.(StateID)].trans[c] {
			F.Add(q)
		}
	}

	return F
}

type DFARow struct {
	set		Set
	trans		map[byte]Set
	accepting	bool
	starting	bool
}

type DFATable []DFARow

func (T DFATable) Get(R Set) map[byte]Set {
	for _, s := range T {
		if R.Equals(s.set) {
			return s.trans
		}
	}
	return nil
}

func (T DFATable) Set(rowID Set, trans map[byte]Set,
	accepting bool, starting bool) DFATable {
	return append(T, DFARow{rowID, trans, accepting, starting})
}

func nfa2dfa(nfa NFA) DFA {
	//NFA==N

	// Let T[row][col] be an empty transitioni table defining D.
	// T[row][.] is uniquely identified by a set of states from N,
	// Each T[.][col] uniquely identifies a character c in Sigma
	T := make(DFATable, 0, 1)

	L := NewStack()

	// let A be the set of accepting states for N
	A := nfa.AcceptingStates()

	tmp := make(Set)
	tmp.Add(StateID(0))
	B := nfa.FollowLambda(tmp)

	trans := make(map[byte]Set)

	for c := range nfa.sigma {
		trans[c.(byte)] = nfa.FollowLambda(nfa.FollowChar(B, c.(byte)))
	}

	T = T.Set(B, trans, B.DoesIntersect(A), true)

	L.Push(B)

	for L.Size() > 0 {
		S, _ := L.Pop()
		for c := range nfa.sigma {
			R := nfa.FollowLambda(nfa.FollowChar(S.(Set), c.(byte)))
			if T.Get(R) == nil {
				// initialize row
				trans := make(map[byte]Set)
				for c := range nfa.sigma {
					trans[c.(byte)] = nfa.FollowLambda(nfa.FollowChar(R, c.(byte)))
				}

				T = T.Set(R, trans, R.DoesIntersect(A), false)
				L.Push(R)
			}
		}
	}

	states := make(map[StateID]*DFAStateMeta, len(T))
	for i := range T {
		trans := make(map[byte]StateID)
		for c, set := range T[i].trans {
			var id StateID
			for j := range T {
				if T[j].set.Equals(set) {
					id = StateID(j)
					break
				}
			}
			trans[c] = id
		}
		states[StateID(i)] = &DFAStateMeta{
			trans: trans,
			accepting: T[i].accepting,
			starting: T[i].starting,
		}
	}

	return DFA{sigma: nfa.sigma, states: states}
}

func dfaTable(dfa DFA, fname string) {
	file, err := os.Create(fname)
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(-1)
	}
	defer file.Close()

	writer := bufio.NewWriter(file)

	chars := make([]byte, 0, dfa.sigma.Size())

	for c := range dfa.sigma {
		chars = append(chars, c.(byte))
	}

	sort.Slice(chars, func (i, j int) bool {
		return chars[i] < chars[j]
	})

	for stateID, stateMeta := range dfa.states {
		if (stateMeta.accepting) {
			fmt.Fprint(writer, "+")
		} else {
			fmt.Fprint(writer, "-")
		}

		fmt.Fprintf(writer, " %d", stateID)

		for _, c := range chars {
			if otherID, ok := stateMeta.trans[c]; ok {
				fmt.Fprintf(writer, " %d", otherID)
			} else {
				fmt.Fprint(writer, " E")
			}
		}
		fmt.Fprintln(writer)
	}

	writer.Flush()
}
