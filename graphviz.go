package main

import (
	"bufio"
	"fmt"
	"os"
)

func WriteDFA(dfa DFA, fname string) {
	file, err := os.Create(fname)
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(-1)
	}
	defer file.Close()

	writer := bufio.NewWriter(file)

	writer.WriteString("digraph {\n")

	if accepting := dfa.AcceptingStates(); accepting.Size() > 0 {
		writer.WriteString("\t{\n")
		for stateID := range accepting {
			fmt.Fprintf(writer,
				"\t\tnode [shape=\"doublecircle\"] %d\n",
				int(stateID.(StateID)))
		}
		writer.WriteString("\t}\n")
	}

	for stateID, stateMeta := range dfa.states {
		for c, neighbor := range stateMeta.trans {
			fmt.Fprintf(writer, "\t%d -> %d [label=\"%c\"]\n",
				int(stateID), int(neighbor), c)
		}
	}

	writer.WriteString("}\n")
	writer.Flush()
}

func WriteNFA(nfa NFA, fname string) {
	file, err := os.Create(fname)
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(-1)
	}
	defer file.Close()

	writer := bufio.NewWriter(file)

	writer.WriteString("digraph {\n")

	if accepting := nfa.AcceptingStates(); accepting.Size() > 0 {
		writer.WriteString("\t{\n")
		for stateID := range accepting {
			fmt.Fprintf(writer,
				"\t\tnode [shape=\"doublecircle\"] %d\n",
				int(stateID.(StateID)))
		}
		writer.WriteString("\t}\n")
	}

	for stateID, stateMeta := range nfa.states {
		for c, neighbors := range stateMeta.trans {
			for _, neighbor := range neighbors {
				fmt.Fprintf(writer,
					"\t%d -> %d [label=\"%c\"]\n",
					int(stateID), int(neighbor), c)
			}
		}
	}

	writer.WriteString("}\n")
	writer.Flush()
}
