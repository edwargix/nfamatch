GV_SOURCES = $(wildcard *.gv)
PNGS = $(GV_SOURCES:.gv=.png)
EXAMPLES = $(wildcard ./examples/*.tar.bz2)
NFAS = $(EXAMPLES:.tar.bz2=)

NFAMATCH: *.go
	go build -o $@

run: NFAMATCH $(NFAS)
	for f in $(NFAS); do \
		./NFAMATCH -g $$f.nfa "$${f##*/}"_ttable.dat; \
	done
	make pngs

grader: nfamatch-student.tar.bz2
	tar xvjf $<

%.png: %.gv
	dot -Tpng $< -o $@

pngs: $(PNGS)

%: %.tar.bz2
	tar xvjf $< -C ./examples

clean:
	rm -rf NFAMATCH $(PNGS) $(GV_SOURCES) ./nfamatch
	for f in $(NFAS); do \
		rm -f "$${f##*/}"_ttable.dat; \
	done

submit: david_jared_nfamatch.tar.gz

david_jared_nfamatch.tar.gz: *.go go.mod Makefile
	tar cvzf $@ $^
